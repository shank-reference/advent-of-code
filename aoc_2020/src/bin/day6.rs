use std::collections::HashSet;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let input: Vec<String> = std::fs::read_to_string("input/day6.in")?
        .split("\n")
        .map(|s| s.to_owned())
        .collect();

    let mut groups: Vec<HashSet<char>> = vec![];
    let mut group = HashSet::new();
    for line in &input {
        if line.is_empty() {
            // start new group
            groups.push(group);
            group = HashSet::new();
        } else {
            // continue with the current group
            for c in line.chars() {
                group.insert(c);
            }
        }
    }
    let result1: usize = groups.iter().map(|g| g.len()).sum();
    println!("part1: {}", result1);

    let mut groups: Vec<HashSet<char>> = vec![];
    group = "abcdefghijklmnopqrstuvwxyz"
        .chars()
        .collect::<HashSet<char>>();
    for line in &input {
        if line.is_empty() {
            // start new group
            groups.push(group.clone());
            group = "abcdefghijklmnopqrstuvwxyz"
                .chars()
                .collect::<HashSet<char>>();
        } else {
            let response = line.chars().collect::<HashSet<char>>();
            group = group.intersection(&response).cloned().collect();
        }
    }
    let result2: usize = groups.iter().map(|g| g.len()).sum();
    println!("part2: {}", result2);

    Ok(())
}

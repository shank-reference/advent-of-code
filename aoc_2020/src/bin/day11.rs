fn main() -> Result<(), Box<dyn std::error::Error>> {
    // let input = std::fs::read_to_string("input/day11_test.in")?;
    let input = std::fs::read_to_string("input/day11.in")?;
    let seats = parse_input(&input);

    let final_seats = simulate_till_stable(seats.clone(), &Part::One);
    let ans1 = count_occupied(&final_seats);
    println!("part1: {}", ans1);

    let final_seats = simulate_till_stable(seats, &Part::Two);
    let ans2 = count_occupied(&final_seats);
    println!("part2: {}", ans2);

    Ok(())
}

#[derive(PartialEq)]
enum Part {
    One,
    Two,
}

fn parse_input(input: &str) -> Vec<Vec<char>> {
    input
        .lines()
        .map(|s| s.chars().collect::<Vec<_>>())
        .collect()
}

fn count_occupied(seats: &[Vec<char>]) -> usize {
    seats
        .iter()
        .map(|row| row.iter().filter(|&c| *c == '#').count())
        .sum()
}

const ADJ: [(isize, isize); 8] = [
    (-1, -1),
    (0, -1),
    (1, -1),
    (-1, 0),
    (1, 0),
    (-1, 1),
    (0, 1),
    (1, 1),
];

fn step(seats: Vec<Vec<char>>, part: &Part) -> Vec<Vec<char>> {
    let cnt = match &part {
        Part::One => 4,
        Part::Two => 5,
    };

    let mut result = vec![];
    for (r, row) in seats.iter().enumerate() {
        let mut updated_row: Vec<char> = vec![];
        for (c, ch) in row.iter().enumerate() {
            let updated_value: char = match *ch {
                'L' => {
                    if adjacent(&seats, r, c, &part)
                        .iter()
                        .all(|neighbour| *neighbour == '.' || *neighbour == 'L')
                    {
                        '#'
                    } else {
                        'L'
                    }
                }
                '#' => {
                    if adjacent(&seats, r, c, &part)
                        .iter()
                        .filter(|&neighbour| *neighbour == '#')
                        .count()
                        >= cnt
                    {
                        'L'
                    } else {
                        '#'
                    }
                }
                ch => ch,
            };
            updated_row.push(updated_value);
        }
        result.push(updated_row);
    }
    result
}

fn adjacent(seats: &[Vec<char>], r: usize, c: usize, part: &Part) -> Vec<char> {
    let max_row = (seats.len() - 1) as isize;
    let max_col = (seats[0].len() - 1) as isize;
    if *part == Part::One {
        [(r, c)]
            .iter()
            .cycle()
            .zip(ADJ.iter())
            .map(|((i, j), (di, dj))| (*i as isize + di, *j as isize + dj))
            .filter(|(i, j)| *i >= 0 && *i <= max_row && *j >= 0 && *j <= max_col)
            .map(|(i, j)| seats[i as usize][j as usize])
            .collect()
    } else {
        let mut result = vec![];
        for (di, dj) in ADJ.iter() {
            let mut ni = r as isize;
            let mut nj = c as isize;
            let mut found = false;
            let mut neighbour = 'x';
            loop {
                ni += di;
                nj += dj;
                if ni < 0 || ni > max_row || nj < 0 || nj > max_col {
                    break;
                }
                if seats[ni as usize][nj as usize] != '.' {
                    found = true;
                    neighbour = seats[ni as usize][nj as usize];
                    break;
                }
            }
            if found {
                result.push(neighbour);
            }
        }
        result
    }
}

fn simulate_till_stable(seats: Vec<Vec<char>>, part: &Part) -> Vec<Vec<char>> {
    let mut seats = seats;
    let mut count = count_occupied(&seats);
    loop {
        seats = step(seats, part);
        let new_count = count_occupied(&seats);
        if new_count == count {
            break;
        }
        count = new_count;
    }
    seats
}

#[test]
fn test_step2() {
    let input = r##"L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL"##;
    let seats = parse_input(input);

    let input = r##"#.##.##.##
#######.##
#.#.#..#..
####.##.##
#.##.##.##
#.#####.##
..#.#.....
##########
#.######.#
#.#####.##"##;
    let updated = parse_input(input);
    assert_eq!(step(seats, &Part::Two), updated);

    let input = r##"#.LL.LL.L#
#LLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLL#
#.LLLLLL.L
#.LLLLL.L#"##;
    let updated2 = parse_input(input);
    assert_eq!(step(updated, &Part::Two), updated2);
}

#[test]
fn test_step() {
    let input = r##"L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL"##;
    let seats = parse_input(input);

    let input = r##"#.##.##.##
#######.##
#.#.#..#..
####.##.##
#.##.##.##
#.#####.##
..#.#.....
##########
#.######.#
#.#####.##"##;
    let updated = parse_input(input);
    assert_eq!(step(seats, &Part::One), updated);

    let input = r##"#.LL.L#.##
#LLLLLL.L#
L.L.L..L..
#LLL.LL.L#
#.LL.LL.LL
#.LLLL#.##
..L.L.....
#LLLLLLLL#
#.LLLLLL.L
#.#LLLL.##"##;
    let updated2 = parse_input(input);
    assert_eq!(step(updated, &Part::One), updated2);
}

#[test]
fn test_count_occupied() {
    let input = r#"L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL"#;

    let seats = parse_input(input);
    assert_eq!(count_occupied(&seats), 0);

    let input = r##"#.#L.L#.##
#LLL#LL.L#
L.#.L..#..
#L##.##.L#
#.#L.LL.LL
#.#L#L#.##
..L.L.....
#L#L##L#L#
#.LLLLLL.L
#.#L#L#.##"##;
    let seats = parse_input(input);
    assert_eq!(count_occupied(&seats), 37);
}

#[test]
fn test_part1() {
    let input = r#"L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL"#;

    let seats = parse_input(input);
    let final_seats = simulate_till_stable(seats, &Part::One);
    assert_eq!(count_occupied(&final_seats), 37);
}

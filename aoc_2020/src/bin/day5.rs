#[macro_use]
extern crate lazy_static;

use aoc2020::get_exec_name;
use std::io::BufRead;
use std::io::BufReader;
use std::{collections::HashSet, fs::File};

lazy_static! {
    static ref ROW_POWERS: Vec<usize> = (0..7).rev().map(|p| 2_usize.pow(p)).collect();
    static ref COL_POWERS: Vec<usize> = (0..3).rev().map(|p| 2_usize.pow(p)).collect();
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let day = get_exec_name().unwrap();

    let input1 = format!("input/{}_1.in", day);
    let input1 = BufReader::new(File::open(input1)?)
        .lines()
        .collect::<Result<Vec<String>, _>>()?;

    let seat_ids = input1.iter().map(calculate_seat_no).collect::<Vec<usize>>();

    println!("part1: {}", part1(&seat_ids));
    println!("part2: {}", part2(&seat_ids));

    Ok(())
}

fn calculate_seat_no(s: &String) -> usize {
    let mux: Vec<usize> = s
        .chars()
        .map(|c| if c == 'F' || c == 'L' { 0 } else { 1 })
        .collect();

    // row no using first 7 chars (0 - 2^7)
    let row: usize = mux
        .iter()
        .zip(ROW_POWERS.iter())
        .map(|(&m, &p)| m * p)
        .sum();

    // column no using last 3 chars
    let col: usize = mux
        .iter()
        .skip(7)
        .zip(COL_POWERS.iter())
        .map(|(&m, p)| m * p)
        .sum();

    // seat no = 8 * row + col
    let seat = 8 * row + col;
    seat
}

#[test]
fn test_calculate_seat_no() {
    let input_output: &[(&String, usize)] = &[
        (&"FBFBBFFRLR".to_string(), 357),
        (&"BFFFBBFRRR".to_string(), 567),
        (&"FFFBBBFRRR".to_string(), 119),
        (&"BBFFBBFRLL".to_string(), 820),
    ];

    for (i, o) in input_output {
        println!("for {}, seat_id should be {}", i, o);
        assert_eq!(calculate_seat_no(i), *o);
    }
}

fn part2(seat_ids: &Vec<usize>) -> usize {
    let min = seat_ids.iter().min().unwrap().to_owned();
    let max = seat_ids.iter().max().unwrap().to_owned();

    let all_seats_set = seat_ids.iter().collect::<HashSet<_>>();

    for i in min..max {
        if (!all_seats_set.contains(&i))
            && all_seats_set.contains(&(i + 1))
            && all_seats_set.contains(&(i - 1))
        {
            return i;
        }
    }
    unreachable!()
}

fn part1(seat_ids: &Vec<usize>) -> usize {
    seat_ids.iter().max().unwrap().clone()
}

use anyhow::Result;
use std::collections::HashMap;

// --- solution

fn main() -> Result<()> {
    let input = parse_input()?;

    println!("part1: {}", part1(&input)?);
    println!("part2: {}", part2(&input)?);
    Ok(())
}
fn part1(starting: &[usize]) -> Result<usize> {
    simulate_upto(starting, 2020)
}

fn part2(starting: &[usize]) -> Result<usize> {
    simulate_upto(starting, 30000000)
}

fn simulate_upto(starting: &[usize], upto: usize) -> Result<usize> {
    let mut lastseen: HashMap<usize, usize> = HashMap::new();
    let mut prev: HashMap<usize, usize> = HashMap::new();

    // simulate the starting turns
    for (turn, n) in starting.iter().enumerate() {
        if lastseen.contains_key(n) {
            prev.insert(*n, lastseen[n]);
        }
        lastseen.insert(*n, turn + 1);
    }

    let mut last = starting[starting.len() - 1];

    // simulate the rest of the turns till 2020
    for turn in starting.len() + 1..upto + 1 {
        let next = match prev.get(&last) {
            Some(p) => lastseen[&last] - p,
            None => 0,
        };
        if let Some(p) = lastseen.get(&next) {
            prev.insert(next, *p);
        }
        lastseen.insert(next, turn);
        last = next;
    }

    Ok(last)
}

#[test]
fn test_part2() {
    let checks: &[(Vec<usize>, usize)] = &[
        (vec![0, 3, 6], 175594),
        // (vec![1, 3, 2], 2578),
        // (vec![2, 1, 3], 3544142),
        // (vec![1, 2, 3], 261214),
        // (vec![2, 3, 1], 6895259),
        // (vec![3, 2, 1], 18),
        // (vec![3, 1, 2], 362),
    ];

    for (input, exp) in checks.iter() {
        assert_eq!(part2(input).unwrap(), *exp);
    }
}

#[test]
fn test_part1() {
    let checks: &[(Vec<usize>, usize)] = &[
        (vec![0, 3, 6], 436),
        (vec![1, 3, 2], 1),
        (vec![2, 1, 3], 10),
        (vec![1, 2, 3], 27),
        (vec![2, 3, 1], 78),
        (vec![3, 2, 1], 438),
        (vec![3, 1, 2], 1836),
    ];

    for (input, exp) in checks.iter() {
        assert_eq!(part1(input).unwrap(), *exp);
    }
}

// --- parse input

fn parse_input() -> Result<Input> {
    // let input = std::fs::read_to_string("input/day15_test.in")?
    let input = std::fs::read_to_string("input/day15.in")?
        .trim()
        .split(',')
        .map(|c| c.parse().unwrap())
        .collect();

    Ok(input)
}

// --- model

type Input = Vec<usize>;

// --- utils

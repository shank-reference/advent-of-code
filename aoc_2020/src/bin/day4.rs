use std::io::{BufRead, BufReader};
use std::{collections::HashMap, fs::File};

use aoc2020::get_exec_name;

const REQUIRED_FIELDS: &[&'static str] = &["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"];
const HAIL_COLOR_CHARS: &[char] = &['a', 'b', 'c', 'd', 'e', 'f'];
const VALID_EYE_COLOURS: &[&'static str] = &["amb", "blu", "brn", "gry", "grn", "hzl", "oth"];

fn parse_hgt(hgt: &String) -> Result<usize, &'static str> {
    if hgt.ends_with("cm") {
        parse_number_in_range(&hgt[0..hgt.len() - 2].to_string(), (150, 193))
    } else if hgt.ends_with("in") {
        parse_number_in_range(&hgt[0..hgt.len() - 2].to_string(), (59, 76))
    } else {
        Err("invalid unit")
    }
}

fn parse_number_in_range(year: &String, range: (usize, usize)) -> Result<usize, &'static str> {
    let (min, max) = range;
    match year.parse::<usize>() {
        Ok(year) => {
            if min <= year && year <= max {
                return Ok(year);
            } else {
                return Err("year out of range");
            }
        }
        Err(_) => Err("invalid year"),
    }
}

fn has_all_required_fields(data: &HashMap<String, String>) -> bool {
    for &field in REQUIRED_FIELDS {
        if !data.contains_key(field) {
            return false;
        }
    }
    true
}

fn is_valid(data: &HashMap<String, String>) -> bool {
    if !has_all_required_fields(data) {
        return false;
    }

    // byr (Birth Year) - four digits; at least 1920 and at most 2002.
    let byr = parse_number_in_range(data.get("byr").unwrap(), (1920, 2002));

    // iyr (Issue Year) - four digits; at least 2010 and at most 2020.
    let iyr = parse_number_in_range(data.get("iyr").unwrap(), (2010, 2020));

    // eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
    let eyr = parse_number_in_range(data.get("eyr").unwrap(), (2020, 2030));

    // hgt (Height) - a number followed by either cm or in:
    //     If cm, the number must be at least 150 and at most 193.
    //     If in, the number must be at least 59 and at most 76.
    let hgt = parse_hgt(data.get("hgt").unwrap());

    // hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
    let hcl = parse_hcl(data.get("hcl").unwrap());

    // ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
    let ecl = parse_ecl(data.get("ecl").unwrap());

    // pid (Passport ID) - a nine-digit number, including leading zeroes.
    let pid = parse_pid(data.get("pid").unwrap());

    if [
        byr.is_err(),
        iyr.is_err(),
        eyr.is_err(),
        hgt.is_err(),
        hcl.is_err(),
        ecl.is_err(),
        pid.is_err(),
    ]
    .iter()
    .any(|v| *v)
    {
        return false;
    } else {
        true
    }
}

fn parse_pid(s: &String) -> Result<&String, &'static str> {
    if s.len() == 9 && s.parse::<usize>().is_ok() {
        Ok(s)
    } else {
        Err("invalid passport id")
    }
}

fn parse_ecl(s: &String) -> Result<&String, &'static str> {
    if VALID_EYE_COLOURS.contains(&s.as_str()) {
        Ok(s)
    } else {
        Err("invalid eye colour")
    }
}

fn parse_hcl(s: &String) -> Result<&String, &'static str> {
    if s.chars().nth(1).map(|c| c == '#').is_some()
        && s.chars().count() == 7
        && s.chars()
            .skip(1)
            .all(|c| c.is_digit(10) || HAIL_COLOR_CHARS.contains(&c))
    {
        Ok(s)
    } else {
        Err("invalid hair colour")
    }
}

fn part1<I>(data: I) -> usize
where
    I: Iterator<Item = std::io::Result<String>>,
{
    // parse one line at a time
    // if the current line is empty, then check if all required attributes are
    // present
    // do the same when end of stream is encountered

    let mut valid = 0;

    let mut current = HashMap::<String, String>::new();
    for line in data {
        let line = line.unwrap();
        if line.is_empty() {
            if has_all_required_fields(&current) {
                valid += 1;
            }
            current.clear();
        } else {
            for parts in line.split(' ') {
                let mut kv = parts.split(':');
                let k = kv.next().unwrap().to_owned();
                let v = kv.next().unwrap().to_owned();
                current.insert(k, v);
            }
        }
    }

    // for the last passport data
    if has_all_required_fields(&current) {
        valid += 1;
    }

    valid
}

fn part2<I>(data: I) -> usize
where
    I: Iterator<Item = std::io::Result<String>>,
{
    // parse one line at a time
    // if the current line is empty, then check if all required attributes are
    // present
    // do the same when end of stream is encountered

    let mut valid = 0;

    let mut current = HashMap::<String, String>::new();
    for line in data {
        let line = line.unwrap();
        if line.is_empty() {
            if is_valid(&current) {
                valid += 1;
            } else {
            }
            current.clear();
        } else {
            for parts in line.split(' ') {
                let mut kv = parts.split(':');
                let k = kv.next().unwrap().to_owned();
                let v = kv.next().unwrap().to_owned();
                current.insert(k, v);
            }
        }
    }

    valid
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let day = get_exec_name().unwrap();

    let input1 = format!("input/{}_1.in", day);
    let input1 = BufReader::new(File::open(input1)?).lines();
    println!("part 1: {}", part1(input1));

    let input2 = format!("input/{}_2.in", day);
    // let input2 = format!("input/{}_test.in", day);
    let input2 = BufReader::new(File::open(input2)?).lines();
    println!("part 2: {}", part2(input2));

    Ok(())
}

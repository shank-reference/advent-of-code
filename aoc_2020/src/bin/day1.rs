/// Make it Work
/// Make it Beautiful
/// Make it Fast
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

// time complexity -> n * log(n)
fn part1(mut data: Vec<u32>) -> u32 {
    data.sort();

    for i in 0..data.len() {
        let to_search = 2020 - data[i];
        match &data[i + 1..].binary_search(&to_search) {
            Ok(idx) => return data[i] * data[i + 1 + idx],
            Err(_) => continue,
        }
    }
    0
}

// time complexity -> n^2 * log(n)
fn part2(mut data: Vec<u64>) -> u64 {
    data.sort();

    for i in 0..data.len() {
        for j in i + 1..data.len() {
            if data[i] + data[j] > 2020 {
                continue;
            }
            let to_search = 2020 - data[i] - data[j];
            match &data[j + 1..].binary_search(&to_search) {
                Ok(idx) => return data[i] * data[j] * data[j + 1 + idx],
                Err(_) => continue,
            }
        }
    }
    0
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let exec_name = aoc2020::get_exec_name().unwrap();

    let input1 = format!("input/{}_1.in", exec_name);
    let input1 = BufReader::new(File::open(input1)?)
        .lines()
        .map(|rs| rs.unwrap())
        .map(|s| s.parse::<u32>().unwrap())
        .collect::<Vec<_>>();

    let input2 = format!("input/{}_2.in", exec_name);
    let input2 = BufReader::new(File::open(input2)?)
        .lines()
        .map(|rs| rs.unwrap())
        .map(|s| s.parse::<u64>().unwrap())
        .collect::<Vec<_>>();

    println!("part1: {}", part1(input1));
    println!("part2: {}", part2(input2));

    Ok(())
}

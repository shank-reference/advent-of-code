defmodule Aoc2018.Day10 do
  defmodule Star do
    @type t :: %Star{x: integer, y: integer, vx: integer, vy: integer}
    defstruct [:x, :y, :vx, :vy]

    @spec new([integer]) :: Star.t()
    def new([pos_x, pos_y, vel_x, vel_y]) do
      %Star{x: pos_x, y: pos_y, vx: vel_x, vy: vel_y}
    end
  end

  @spec step(Star.t()) :: Star.t()
  def step(star) do
    %Star{star | x: star.x + star.vx, y: star.y + star.vy}
  end

  @spec step_back(Star.t()) :: Star.t()
  defp step_back(star) do
    %Star{star | x: star.x - star.vx, y: star.y - star.vy}
  end

  @spec grid([Star.t()]) :: String.t()
  def grid(stars) do
    {min_x, max_x, min_y, max_y} = get_bounds(stars)

    star_locations = Enum.map(stars, &{&1.x, &1.y}) |> MapSet.new()

    gridlines =
      Enum.map(min_y..max_y, fn y ->
        Enum.map(min_x..max_x, fn x ->
          if {x, y} in star_locations do
            "#"
          else
            "."
          end
        end)
      end)

    Enum.join(gridlines, "\n")
  end

  @spec get_bounds([Star.t()]) :: {integer, integer, integer, integer}
  defp get_bounds(stars) do
    {%Star{x: min_x}, %Star{x: max_x}} = Enum.min_max_by(stars, fn star -> star.x end)
    {%Star{y: min_y}, %Star{y: max_y}} = Enum.min_max_by(stars, fn star -> star.y end)

    {min_x, max_x, min_y, max_y}
  end

  @spec grid_size([Star.t()]) :: non_neg_integer
  defp grid_size(stars) do
    {min_x, max_x, min_y, max_y} = get_bounds(stars)

    (max_x - min_x) * (max_y - min_y)
  end

  @spec parse_input() :: [Star.t()]
  defp parse_input() do
    File.read!("input/day_10.txt")
    |> String.trim()
    |> String.split("\n", trim: true)
    |> Enum.map(&create_star/1)
  end

  @spec create_star(String.t()) :: Star.t()
  defp create_star(line) do
    line
    |> String.split(["position=<", ",", "> velocity=<", ",", ">"], trim: true)
    |> Enum.map(fn v -> v |> String.trim() |> String.to_integer() end)
    |> Star.new()
  end

  @spec main() :: :ok
  def main() do
    stars = parse_input()

    {message, time} = find_message_and_time(stars)
    IO.puts("part 1: \n#{message}")
    IO.puts("part 2: #{time}")
  end

  @spec find_message_and_time([Star.t()]) :: {String.t(), integer}
  defp find_message_and_time(stars) do
    do_find_message_and_time(stars)
  end

  @spec do_find_message_and_time([Star.t()]) :: {String.t(), integer}
  defp do_find_message_and_time(stars), do: do_find_message_and_time(stars, grid_size(stars), 0)

  @spec do_find_message_and_time([Star.t()], integer, integer) :: {String.t(), integer}
  defp do_find_message_and_time(stars, max_gridsize, time) do
    stars = Enum.map(stars, &step/1)
    new_gridsize = grid_size(stars)

    if new_gridsize > max_gridsize do
      {grid(stars |> Enum.map(&step_back/1)), time}
    else
      do_find_message_and_time(stars, new_gridsize, time + 1)
    end
  end
end

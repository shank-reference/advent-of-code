defmodule Aoc2018.Day3 do
  @moduledoc """
  Documentation for Day3.
  """

  @typedoc """
  "#123 @ 3,2: 5x4"
  """
  @type unparsed_claim :: String.t()

  @typedoc """
  {claim_id, left, top, width, height}
  """
  @type claim ::
          {non_neg_integer, non_neg_integer, non_neg_integer, non_neg_integer, non_neg_integer}

  @typedoc """
  "{3, 5}"
  """
  @type coordinate :: {non_neg_integer, non_neg_integer}

  # Notes to self
  # -------------
  # - The fabric is 1000x1000

  @spec input() :: [unparsed_claim]
  def input() do
    File.read!("input/day_3.txt")
    |> String.trim()
    |> String.split("\n")
  end

  @spec main() :: :ok
  def main() do
    input()
    |> Enum.map(&parse_claim/1)
    |> part1()
    |> IO.puts()

    input()
    |> Enum.map(&parse_claim/1)
    |> part2()
    |> IO.puts()
  end

  @doc """
  Parse claim string.

  ## Examples

      iex> Day3.parse_claim("#1 @ 1,3: 4x4")
      {1, 1, 3, 4, 4}

  """
  @spec parse_claim(unparsed_claim) :: claim
  def parse_claim(unparsed_claim) do
    unparsed_claim
    |> String.split(["#", " @ ", ",", ": ", "x"])
    |> Enum.drop(1)
    |> Enum.map(&String.to_integer/1)
    |> List.to_tuple()
  end

  @doc """
  Apply claim to the fabric.

  ## Examples

      iex> Map.get(
      ...>   Day3.do_claims([
      ...>     {1, 1, 3, 4, 4},
      ...>     {2, 3, 1, 4, 4},
      ...>     {3, 5, 5, 2, 2}
      ...>   ]),
      ...>   {3, 1}
      ...> )
      #MapSet<[2]>


      iex> Map.get(
      ...>   Day3.do_claims([
      ...>     {1, 1, 3, 4, 4},
      ...>     {2, 3, 1, 4, 4},
      ...>     {3, 5, 5, 2, 2}
      ...>   ]),
      ...>   {1, 3}
      ...> )
      #MapSet<[1]>

      iex> Map.get(
      ...>   Day3.do_claims([
      ...>     {1, 1, 3, 4, 4},
      ...>     {2, 3, 1, 4, 4},
      ...>     {3, 5, 5, 2, 2}
      ...>   ]),
      ...>   {3, 3}
      ...> )
      #MapSet<[1, 2]>

  """
  @spec do_claims([claim]) :: %{coordinate => [integer]}
  def do_claims(claims) do
    Enum.reduce(claims, %{}, fn {claim_id, left, top, width, height}, acc ->
      # reduce from left..(left+width) and top..(top+height)
      Enum.reduce(left..(left + width - 1), acc, fn x, acc ->
        Enum.reduce(top..(top + height - 1), acc, fn y, acc ->
          Map.update(acc, {x, y}, MapSet.new([claim_id]), fn claimed_ids ->
            MapSet.put(claimed_ids, claim_id)
          end)
        end)
      end)
    end)
  end

  @doc """
  Apply claim to the fabric.

  ## Examples

      iex> Day3.part1([
      ...> {1, 1, 3, 4, 4},
      ...> {2, 3, 1, 4, 4},
      ...> {3, 5, 5, 2, 2}
      ...> ])
      4

  """
  @spec part1([claim]) :: non_neg_integer
  def part1(claims) do
    claims
    |> do_claims
    |> Enum.count(fn {_coordinate, claims} -> MapSet.size(claims) >= 2 end)
  end

  @doc """
  Apply claim to the fabric.

  ## Examples

      iex> Day3.part2([
      ...> {1, 1, 3, 4, 4},
      ...> {2, 3, 1, 4, 4},
      ...> {3, 5, 5, 2, 2}
      ...> ])
      3

  """
  @spec part2([claim]) :: non_neg_integer
  def part2(claims) do
    # TODO
    applied = claims |> do_claims

    # Set of all the claim ids
    all_ids =
      Enum.reduce(claims, MapSet.new(), fn {id, _, _, _, _}, acc ->
        MapSet.put(acc, id)
      end)

    # Set of claim ids that appear in overlapped claims
    overlapped_claim_ids =
      Enum.reduce(applied, MapSet.new(), fn {_coordinate, claim_ids}, acc ->
        if MapSet.size(claim_ids) >= 2 do
          Enum.reduce(claim_ids, acc, fn id, acc ->
            MapSet.put(acc, id)
          end)
        else
          acc
        end
      end)

    # Diff the two
    MapSet.difference(all_ids, overlapped_claim_ids) |> MapSet.to_list() |> List.first()
  end
end

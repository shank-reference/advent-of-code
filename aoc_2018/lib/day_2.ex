defmodule Aoc2018.Day2 do
  @type box_id() :: String.t()

  @spec input() :: [box_id]
  def input() do
    File.read!("input/day_2.txt")
    |> String.trim()
    |> String.split("\n")
  end

  @spec main() :: :ok
  def main() do
    input()
    |> part1()
    |> IO.puts()

    input()
    |> part2()
    |> IO.puts()
  end

  @doc """
  Part1.

  ## Examples

      iex> Day2.part1([
      ...>   "abcdef",
      ...>   "bababc",
      ...>   "abbcde",
      ...>   "abcccd",
      ...>   "aabcdd",
      ...>   "abcdee",
      ...>   "ababab"
      ...> ])
      12

  """
  @spec part1([box_id]) :: non_neg_integer()
  def part1(ids) do
    id_char_counts =
      ids
      |> Enum.map(fn id ->
        id
        |> String.codepoints()
        |> Enum.reduce(%{}, fn c, acc ->
          Map.update(acc, c, 1, &(&1 + 1))
        end)
      end)

    twos =
      Enum.count(id_char_counts, fn id_char_count ->
        Enum.any?(id_char_count, fn {_c, cnt} -> cnt == 2 end)
      end)

    threes =
      Enum.count(id_char_counts, fn id_char_count ->
        Enum.any?(id_char_count, fn {_c, cnt} -> cnt == 3 end)
      end)

    twos * threes
  end

  @doc """
  Part2.

  ## Examples

      iex> Day2.part2([
      ...>   "abcde",
      ...>   "fghij",
      ...>   "klmno",
      ...>   "pqrst",
      ...>   "fguij",
      ...>   "axcye",
      ...>   "wvxyz"
      ...> ])
      "fgij"

  """
  @spec part2([box_id]) :: String.t()
  def part2(ids) do
    # make it work
    # make it pretty
    # make it fast
    Enum.reduce_while(ids, nil, fn id, _acc ->
      case Enum.find(ids, fn other -> one_char_diff(id, other) end) do
        nil ->
          {:cont, nil}

        one_diff ->
          common =
            String.codepoints(id)
            |> Enum.zip(String.codepoints(one_diff))
            |> Enum.filter(fn {c1, c2} -> c1 == c2 end)
            |> Enum.map(fn {c1, _c2} -> c1 end)
            |> Enum.join()

          {:halt, common}
      end
    end)
  end

  @spec one_char_diff(box_id, box_id) :: boolean()
  def one_char_diff(id1, id2) do
    diffs =
      id1
      |> String.codepoints()
      |> Enum.zip(String.codepoints(id2))
      |> Enum.count(fn {c1, c2} -> c1 != c2 end)

    diffs == 1
  end
end
